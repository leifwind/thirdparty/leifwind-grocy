FROM ghcr.io/linuxserver/grocy:v3.1.1-ls121

RUN chown -R abc:abc /app && \
    mkdir -p /var/lib/nginx /config /var/tmp/nginx && \
    chown -R abc:abc \
    /config \
    /var/lib/nginx \
    /var/tmp/nginx
USER abc
